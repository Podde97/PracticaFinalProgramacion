package pokemon2netbeans;

/**
 * Excepcion que evita que se rebase el limite de pokemons en el array
 * @author Miguel Dominguez
 */
public class MochilaLlenaExcepcion extends Exception {
	@Override
	public String toString ( ) {
        return "La mochila esta llena";
    }

}