package pokemon2netbeans;
/**
 * La clase del personaje del usuario, contiene una mochila y unos pokecuartos, y el nombre, genero,y fecha de nacimiento
 * identifica al personaje del usuario.
 * @author Miguel Dominguez
 */
public class Personaje {

    private int codigo;
    private Mochila mochila;
    private final String name;
    private final String gender;
    private final String date;
    private int pokeCuartos = 0;
    /**
     * El constructor de la clase personaje.
     * @param name El nombre del personaje.
     * @param genero El genero del personaje.
     * @param fechaNacimiento la fecha de nacimiento del personaje.
     */
    public Personaje(String name, String genero, String fechaNacimiento) {
        this.name = name;
        this.gender = genero;
        this.date = fechaNacimiento;
    }
    /**
     * Retorna el nombre
     * @return 
     */
    public String getName() {
        return name;
    }
/**
 * retorna el genero
 * @return 
 */
    public String getGender() {
        return gender;
    }

    /**
     * retorna los pokeCuartos
     * @return 
     */
    public int getPokeCuartos() {
        return pokeCuartos;
    }
    /**
     * retorna el codigo del personaje
     * @return 
     */
    public int getCodigo() {
        return codigo;
    }
    /**
     * metodo para cargar el codigo (desde la base de datos)
     * @param codigo el codigo del personaje
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * carga una variable de tipo mochila (que puede contener cosas dentro) y la convierte en variable del personaje.
     * @param mochila La mochila del personaje.
     */
    public void setMochila(Mochila mochila) {
        this.mochila = mochila;
    }
    /**
     * retorna el nombre del personaje
     * @return 
     */
    public String getNombre() {
        return this.name;
    }
     /**
      * retorna el genero del personaje.
      * @return 
      */
    public String getGenero() {
        return this.gender;
    }
     /**
      * retorna la fecha de nacimiento 
      * @return 
      */
    public String getFecha() {
        return this.date;
    }

     /**
      * carga los pokeCuartos del personaje (desde la base de datos).
      * @param cuartos 
      */
    public void setCuartos(int cuartos) {

        this.pokeCuartos = cuartos;
    }
     /**
      * cambia el valor de la variable monetaria (cuando ganas dinero, o lo gastas).
      * @param diferencia lo que se suma (o se resta) para hacer el nuevo valor de pokeCuartos.
      */
    public void setEconomia(int diferencia) {
        this.pokeCuartos = pokeCuartos + diferencia;
    }
     /**
      * Carga el codigo (desde la base de datos).
      * @param codigo 
      */
    public void setCodigo(String codigo) {
        this.codigo = Integer.parseInt(codigo);
    }
     /**
      * retorna la mochila.
      * @return 
      */
    public Mochila getMochila() {
        return this.mochila;
    }

}
