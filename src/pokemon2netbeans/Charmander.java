package pokemon2netbeans;
/**
 * La clase Charmander, heredera de atributos y metodos de Pokemon.
 * @author Miguel Dominguez
 */
public class Charmander extends Pokemon {

    /**
     * El constructor de la clase Charmander (stats basicos).
     *
     * @param salud la salud base del pokemon.
     * @param ataque el ataque base del pokemon.
     * @param defensa la defensa base del pokemon.
     * @param atEspecial el ataque especial del pokemon.
     * @param defEspecial la defensa especial del pokemon.
     * @param velocidad la velocidad del pokemon.
     */
    public Charmander(int salud, int ataque, int defensa, int atEspecial, int defEspecial, int velocidad) {

        this.salud = salud;
        this.ataque = ataque;
        this.defensa = defensa;
        this.atEspecial = atEspecial;
        this.defEspecial = defEspecial;
        this.velocidad = velocidad;
    }

    /**
     * Constructor con mote
     * 
     * @param nivel el nivel que se atribuye al pokemon
     * @param mote el mote que se atribuye al pokemon
     * @param salud la salud base del pokemon.
     * @param ataque el ataque base del pokemon.
     * @param defensa la defensa base del pokemon.
     * @param atEspecial el ataque especial del pokemon.
     * @param defEspecial la defensa especial del pokemon.
     * @param velocidad la velocidad del pokemon.
     */
    public Charmander(int nivel, String mote, int salud, int ataque, int defensa, int atEspecial, int defEspecial, int velocidad) {
        this.nivel = nivel;
        this.salud = salud + ((nivel * naturaleza));
        this.ataque = ataque + ((nivel * naturaleza ));
        this.defensa = defensa + ((nivel * naturaleza));
        this.atEspecial = atEspecial + ((nivel * naturaleza));
        this.defEspecial = defEspecial + (nivel * naturaleza);
        this.velocidad = velocidad + ((nivel * naturaleza ));
        this.mote = mote;
        this.nombre = "Charmander";
        this.especie = "CHARMANDER";
        this.tipo = new String[]{"Fuego"};
    }

    /**
     * Constructor sin mote
     * 
     * @param nivel
     * @param salud la salud base del pokemon.
     * @param ataque el ataque base del pokemon.
     * @param defensa la defensa base del pokemon.
     * @param atEspecial el ataque especial del pokemon.
     * @param defEspecial la defensa especial del pokemon.
     * @param velocidad la velocidad del pokemon.
     */

    public Charmander(int nivel, int salud, int ataque, int defensa, int atEspecial, int defEspecial, int velocidad) {
        this.nivel = nivel;
        this.nombre = "Charmander";
        this.salud = salud + ((nivel * naturaleza));
        this.ataque = ataque + ((nivel * naturaleza ));
        this.defensa = defensa + ((nivel * naturaleza));
        this.atEspecial = atEspecial + ((nivel * naturaleza));
        this.defEspecial = defEspecial + (nivel * naturaleza);
        this.velocidad = velocidad + ((nivel * naturaleza ));
        this.mote = nombre;
        this.especie = "CHARMANDER";
        this.tipo = new String[]{"Fuego"};
    }

}
