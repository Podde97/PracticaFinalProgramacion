package pokemon2netbeans;

/**
 * La clase mochila que contiene todos los pokemons, las pokeballs, los pokemons que hay ahora, los huecos que pueden
 * llenar mas pokemons y las pociones.
 * 
 * Se inicializan las pokeballs a 10.
 * @author Miguel Dominguez
 */
public class Mochila {

    private int contador;
    private final Pokemon[] equipo;
    private int pokeballs = 10;
    private int pociones = 0;
    /**
     * El constructor de mochila, empezará con 0 pokemons, y un array de 100 pokemons, es decir, que tendrá un maximo
     * de 100 pokemons.
     */
    public Mochila() {
        this.contador = 0;
        this.equipo = new Pokemon[100];
    }
    /**
     * Metodo para retornar los pokemons de la Partida.
     * @return Los pokemons de la partida.
     */
    public Pokemon[] getEquipo() {
        return this.equipo;
    }
    /**
     * Metodo para aumentar el contador (variable que determina cuantos pokemons hay en la mochila).
     * @param aumento Cuanto aumenta el contador.
     */
    public void setContador(int aumento) {
        this.contador = aumento;
    }
    /**
     * Metodo que retorna el contador.
     * @return el contador de los pokemons que hay en el array.
     */
    public int getContador() {
        return contador;
    }
    /**
     * metodo que se usa para verificar que el pokemon capturado no rebasa el numero maximo de pokemons
     * de la mochila (para que el programa no falle).
     * 
     * @param pokemon El pokemon capturado.
     * @throws MochilaLlenaExcepcion Excepcion que controla el sobrepaso del array de los pokemons.
     */
    public void capturarPokemon(Pokemon pokemon) throws MochilaLlenaExcepcion {
        if (contador > 99) {
            throw new MochilaLlenaExcepcion();
        } else {
            equipo[contador] = pokemon;
            contador++;
        }
    }
    /**
     * Metodo para retornar un pokemon especifico.
     * @param posicion la posicion del pokemon en el array.
     * @return el pokemon de la posicion elegida.
     */
    public Pokemon getPokemon(int posicion) {
        return equipo[posicion];
    }
    /**
     * El metodo para liberar un pokemon especifico.
     * @param posicion la posicion del pokemon que se desea eliminar.
     */
    public void liberaPokemon(int posicion) {
        equipo[posicion] = null;
        for (int i = posicion; i < contador; i++) {
            if ((i + 1) < contador) {
                equipo[i] = equipo[i + 1];
            } else {
                equipo[i] = null;
            }
        }
        contador--;
    }
    /**
     * metodo que retorna la cantidad de pokemons que aun caben en la mochila sin rebasar el limite.
     * @return los huecos que faltan en la mochila.
     */
    public int getHuecos() {
        int huecos = 0;
        for (Pokemon equipo1 : equipo) {
            if (equipo1 == null) {
                huecos++;
            }
        }
        return huecos;
    }
    /**
     * Metodo que sirve para cambiar el numero de pokeballs (cuando se usan o se compran).
     * @param diferencia lo que se suma a las pokeballs para dar la nueva cantidad.
     */
    public void setPokeball(int diferencia) {
        this.pokeballs = pokeballs + diferencia;
    }
    /**
     * metodo que retorna el numero de pokeballs que hay.
     * @return el numero de pokeballs del jugador.
     */
    public int getPokeball() {
        return this.pokeballs;
    }
    /**
     * Metodo para cargar un pokemon en un hueco especifico (al cargar partida desde base de datos).
     * @param hueco la posicion del pokemon.
     * @param aux el pokemon que se recibe para introducirse en el array.
     */
    public void cargarPokemon(int hueco, Pokemon aux) {
        this.equipo[hueco] = aux;
    }
    /**
     * metodo para cargar las pokeballs (desde la base de datos).
     * @param pokeballs 
     */
    public void cargarPokeball(int pokeballs) {
        this.pokeballs = pokeballs;
    }
    /**
     * metodo para cargar las pociones (desde la base de datos).
     * @param pociones 
     */
    public void cargarPociones(int pociones) {
        this.pociones = pociones;
    }
    /**
     * metodo que retorna el numero de pociones que hay.
     * @return el numero de pociones del jugador.
     */
    public int getPociones() {
        return pociones;
    }
}
