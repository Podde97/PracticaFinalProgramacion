package pokemon2netbeans;

/**
 * Estamos ante una aplicación de POKEMON, Mediante una conexion jdbc se
 * guardarán las partidas en la base de datos (presente en la carpeta Partidas
 * guardadas). El programa creara pokemons cuando el jugador los "captura" y les
 * otorgara un nivel aleatorio y unas estadisticas basadas en el nivel y las
 * bases estadisticas del pokemon (que estaran guardadas en la carpeta
 * Pokebases).
 *
 * Como aplicaciones adicionales, el jugador podrá Listar los pokemons, y dentro
 * de ese menú podra liberar los pokemons para ganar mas pokeCuartos$ y no
 * perder la partida (que acabará cuando el jugador se queda sin pokeCuartos ni
 * pokeballs), listar los pokemons por nivel, o mostrar mas en detalle las
 * estadisticas de un pokemon en particular.
 *
 * En un tercer menú, el jugador vera sus estadisticas, pokeballs, pociones (que
 * actualmente no sirven para nada),pokeCuartos$, y los atributos del jugador
 * (nombre, fecha de nacimiento y genero).
 *
 * Por ultimo, el ultimo menú es la tienda, cuando se acaban las pokeballs o las
 * pociones (no implementadas) podrás comprar mas usando los pokeCuartos que
 * ganes al liberar y al capturar pokemons, para seguir dentro de la partida.
 *
 * Se iran implementando funciones a lo largo del verano.
 *
 * @author Miguel Dominguez
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Menu {

    static Scanner input = new Scanner(System.in);
    static Personaje entrenador;
    static Mochila mochila;
    static File file;

    public static void main(String[] args) {
        Connection con = establecerConexion();
        if (con == null) {
            System.out.println("Jugaras sin poder guardar ni cargar las partidas");
        }
        System.out.println("BIENVENIDO AL MUNDO DE POKEMON!!!");

        System.out.println("Que deseas hacer?");
        System.out.println();
        System.out.println("1 - Nueva partida");
        System.out.println("2 - Cargar partida");
        int decision = input.nextInt();
        switch (decision) {
            case 1:
                entrenador = nuevaPartida();
                mochila = new Mochila();
                entrenador.setMochila(mochila);
                mochila.setContador(0);
                break;
            case 2:
                entrenador = cargarPartida(input, con);
                 {
                    try {
                        mochila = cargarMochila(con, entrenador.getCodigo());
                    } catch (FileNotFoundException ex) {
                        System.out.println("error al intentar cargar el archivo");
                    }
                }
                entrenador.setMochila(mochila);
                mochila.setContador(100 - mochila.getHuecos());
        }

        menu();
        int opcion = 0;
        boolean errorM = true;
        while (errorM) {
            try {
                opcion = input.nextInt();
                errorM = false;
            } catch (java.util.InputMismatchException menuError) {
                System.out.println("Introduce un valor valido");
                input.nextLine();
            }
        }
        boolean ejecucion = true;
        while (ejecucion) {
            switch (opcion) {
                case 0:
                    break;
                case 1:
                    input.nextLine();
                    boolean repetir = true;
                    while (repetir) {
                        double probabilidad = Math.random();
                        int pokeballs = mochila.getPokeball();
                        if ((pokeballs != 0) && (probabilidad >= 0.6666666666667)) {
                            try {

                                mochila.capturarPokemon(capturarPkm());
                                mochila.setPokeball(-1);
                                Pokemon referenciaPk = mochila.getPokemon((mochila.getContador()) - 1);
                                System.out.println("Quieres darle un mote S/N");
                                String respuesta = input.nextLine();
                                if (respuesta.equalsIgnoreCase("si") || respuesta.equalsIgnoreCase("s")) {
                                    System.out.println("Como quieres que se llame ?");
                                    String motePk = input.nextLine();
                                    referenciaPk.setMote(motePk);
                                }
                                System.out.println("Has capturado un Pokemon!!!");
                                System.out.println("=================");
                                System.out.println("Especie : " + referenciaPk.getEspecie());
                                System.out.println("Nivel : " + referenciaPk.getNivel());
                                System.out.println("Mote : " + referenciaPk.getMote());
                                entrenador.setEconomia(300);
                                System.out.println("--------- Se han añadido 300$ ---------");
                            } catch (MochilaLlenaExcepcion errorMochila) {
                                System.out.println(errorMochila.toString());
                            }
                            repetir = false;
                        }
                        if (pokeballs == 0) {
                            System.out.println("Vaya, al parecer no tienes pokeballs suficientes, puedes comprar mas en tienda");
                            repetir = false;
                        }
                        if ((probabilidad < 0.6666666666667) && (pokeballs != 0)) {
                            System.out.println("CASI, la captura ha fallado, tendras que intentar otra vez.");
                            mochila.setPokeball(-1);
                            System.out.println("deseas volver a intentarlo?");
                            System.out.println("");
                            System.out.println("Si - vuelve a intentar");
                            System.out.println("Otra combinación de teclas - Salir al menu");
                            String reintentar = input.nextLine();
                            if (reintentar.equalsIgnoreCase("si")) {
                            } else {
                                repetir = false;
                            }

                        }
                    }
                    break;
                case 2:
                    int contador = mochila.getContador();
                    System.out.println("Pokemon");
                    System.out.println("==================");
                    for (int i = 0; i < contador; i++) {
                        Pokemon temporal = mochila.getPokemon(i);
                        System.out.println((i + 1) + " " + temporal.getMote() + "(" + temporal.getNombre() + ")-lvl (" + temporal.getNivel() + ")");
                    }
                    boolean seguirMostrando = true;
                    while (seguirMostrando) {
                        System.out.print("Opciones(M- Mostrar; L-Liberar; V-Volver; O-Ordenar): ");
                        char opciones = input.next().charAt(0);

                        switch (opciones) {
                            case 'M':
                            case 'm':
                                try {
                                    mostrarPkm(mochila);
                                } catch (java.lang.ArrayIndexOutOfBoundsException mostrarPk) {
                                    System.out.println("No hay Pokemon en esa posicion");
                                } catch (java.lang.NullPointerException errorAlMostrar) {
                                    System.out.println("Has puesto una posicion incorrecta");
                                }
                                break;
                            case 'L':
                            case 'l':
                                System.out.println("Escribe la posicion del pokemon a liberar: ");
                                input.nextLine();
                                int liberar = input.nextInt();
                                if (liberar <= mochila.getContador()) {
                                    try {
                                        mochila.liberaPokemon(liberar - 1);
                                        entrenador.setEconomia(100);
                                        System.out.println("--------- Se han añadido 100$ ---------");
                                    } catch (java.lang.ArrayIndexOutOfBoundsException liberarPk) {
                                        System.out.println("No hay Pokemon en esa posicion");
                                    }
                                }
                                break;
                            case 'O':
                            case 'o':
                                System.out.println("te gustaria ordenarlo por nivel o por fecha de captura?");
                                System.out.println("N - Nivel;");
                                char ordenar = input.next().charAt(0);
                                switch (ordenar) {
                                    case 'N':
                                    case 'n':
                                        ordenarNivel(mochila.getEquipo(), mochila.getContador());
                                        break;
                                }
                                System.out.println(" LISTO !!!!");
                                break;
                            default:
                                seguirMostrando = false;
                                System.out.println("Volviendo al menu principal");
                                break;
                        }
                    }

                    break;

                case 3:
                    System.out.println("--------------------Entrenador");
                    System.out.println("Nombre: " + entrenador.getNombre());
                    System.out.println("Genero: " + entrenador.getGenero());
                    System.out.println("Fecha de nacimiento: " + entrenador.getFecha());
                    System.out.println("PokeCuartos: " + entrenador.getPokeCuartos() + "$");
                    System.out.println("Pokeballs restantes: " + mochila.getPokeball());
                    System.out.println("Pociones restantes: " + mochila.getPociones());
                    System.out.println("Huecos en mochila: " + mochila.getHuecos());
                    System.out.println();
                    System.out.print("Opciones(Guardar - G; Volver - OTRA): ");
                    char opciones = input.next().charAt(0);

                    switch (opciones) {
                        case 'G':
                        case 'g':
                            boolean exito = guardarPartida(entrenador);
                            if (exito) {
                                System.out.println("La partida se ha guardado con exito");
                            } else {
                                System.out.println("Ha habido un error de guardado...No se ha guardado con exito");
                            }

                            break;

                        default:
                            System.out.println("Volviendo al menu principal");
                            break;
                    }
                    break;
                case 4:
                    menuTienda();
                    int pokeballs = mochila.getPokeball();
                    int compra = input.nextInt();
                    switch (compra) {
                        case 1:
                            int dinero = entrenador.getPokeCuartos();
                            System.out.println("cuantas Pokeballs querrias comprar?");
                            int cantidad = input.nextInt();
                            if ((dinero >= 100 * cantidad) && (cantidad != 0) && (cantidad <= (50 - pokeballs))) {
                                mochila.setPokeball(cantidad);
                                entrenador.setEconomia(-(100 * cantidad));
                                System.out.println("MUCHAS GRACIAS POR SU COMPRA n.n");
                            }
                            if ((dinero < 100 * cantidad) && (cantidad != 0) && (cantidad <= (50 - pokeballs))) {
                                System.out.println("Lo siento " + entrenador.getNombre() + " pero no parece que tengas PokeCuartos para esta transaccion :(");
                            }
                            if (cantidad == 0) {
                                System.out.println("Bien, vuelva pronto");
                            }
                            if (pokeballs == 50) {
                                System.out.println("desgraciadamente tienes demasiadas pokeballs, deberias usarlas, SON PARA USARLAS >:V");
                            }
                            if ((cantidad > (50 - pokeballs) && (pokeballs < 50))) {
                                System.out.println("Vaya, por reglas de politica interna no puedes llevar mas de 50 pokeballs (aguafiestas)");
                            }

                    }
            }
            if (opcion != 0) {
                menu();
                boolean errorMenu = true;
                while (errorMenu) {
                    try {
                        opcion = input.nextInt();
                        errorMenu = false;
                    } catch (java.util.InputMismatchException menuError) {
                        System.out.println("Introduce un valor valido");
                        input.nextLine();
                    }
                }

            }
            if (opcion == 0) {
                ejecucion = false;
            }
        }
    }

    /**
     * El metodo de JDBC para conectar con la base de datos para guardar,
     * cargar, o comparar datos en,desde, y/o con la base de datos.
     *
     * @return Devuelve la conexion con la base de datos para su uso.
     */
    public static Connection establecerConexion() {
        String db = "Pokemon";
        Connection con = null;
        Statement query;
        ResultSet result;
        try {
            con = DriverManager.getConnection(
                    "jdbc:mysql://192.168.2.102/" + db + "?serverTimezone=UTC", "Podde97", "Miguel@1");

            System.out.println("Conexion establecida");

        } catch (SQLException ex) {
            System.out.println("Error en la conexion con " + db + ": " + ex.toString());
        }
        return con;
    }

    /**
     * Es el metodo que se usará para crear un personaje nuevo, que
     * posteriormente tendra una mochila(linea 68) que a su vez esta contendra
     * los pokemons y los consumibles.
     *
     * Este personaje será el "avatar identificador" del usuario.
     *
     * @return El nuevo personaje del usuario.
     */
    public static Personaje nuevaPartida() {
        int codigo = 1;
        System.out.println("Empezemos por ti, crearemos un Pokeentrenador respondiendo a estas preguntas");
        System.out.println("Empezemos por tu nombre:");
        input.nextLine();
        String name = input.nextLine();
        System.out.println("Esplendido " + name + ", ahora necesitaré saber tu genero (H/M)");
        String gender = input.nextLine();
        System.out.println("genial, por ultimo tendras que decirme tu fecha de nacimiento en este formato (yyyy-mm-dd):");
        String date = input.nextLine();
        System.out.println("Perfecto, que te diviertas! n.n");
        Personaje entrenadorNuevo = new Personaje(name, gender, date);
        try {
            Connection con = establecerConexion();
            Statement query = con.createStatement();
            ResultSet result = query.executeQuery("Select id FROM Mochilas");
            while (result.next()) {
                codigo++;
            }
            result.close();
            query.close();
            con.close();
        } catch (SQLException ex) {
            System.out.println("No se ha podido obtener el codigo");
            codigo = Integer.parseInt("Unknown");
        }
        entrenadorNuevo.setCodigo(codigo);
        return entrenadorNuevo;
    }

    /**
     * Este metodo sirve para cargar la partida desde la base de datos y
     * requiere de una conexion a la base de datos (que conseguira tras el
     * metodo "establecerConexion").
     *
     * mediante los ResultSet de las distintas querys, el programa almacenara en
     * las variables del personaje los resultados que devolverá la base de
     * datos, para que el programa tenga una vez cerrado el programa, los datos
     * del usuario de vuelta.
     *
     * @param input El medio de entrada de datos (teclado) del usuario (sirve
     * para introducir el num de partida).
     * @param con La variable que almacena la conexion con la base de datos.
     * @return El personaje guardado en la base de datos que se devolvera como
     * el personaje de la partida.
     */
    public static Personaje cargarPartida(Scanner input, Connection con) {
        Personaje entrenador = null;
        Mochila mochila = null;
        ResultSet result;
        Statement query;
        try {
            query = con.createStatement();
            System.out.println("Que partida quieres cargar?");
            result = query.executeQuery("Select id, nombre FROM Mochilas ORDER BY id;");
            while (result.next()) {
                System.out.println(result.getString("id") + ": -" + result.getString("Nombre"));
            }
            int decision = input.nextInt();
            //entrenador
            result = query.executeQuery("Select * FROM Mochilas WHERE id=" + decision + ";");
            while (result.next()) {
                entrenador = new Personaje(result.getString("Nombre"),
                        result.getString("genero"),
                        result.getString("fechaNacimiento"));
            }
            result = query.executeQuery("SELECT id FROM Mochilas WHERE id=" + decision + ";");
            while (result.next()) {
                entrenador.setCodigo(result.getInt("id"));
            }
            result = query.executeQuery("SELECT pokeCuartos FROM Mochilas WHERE id=" + decision + ";");
            while (result.next()) {
                entrenador.setCuartos(result.getInt("pokeCuartos"));
            }
            result.close();
            query.close();

        } catch (SQLException ex) {
            System.out.println("Error en la conexion con pokemon " + ex.toString());
        }
        if (mochila == null) {
            System.out.println("NO HAY MOCHILA");
        }
        return entrenador;
    }

    //mochila
    /**
     * Exactamente el mismo funcionamiento que el metodo anterior (Cargar
     * Partida) pero con los datos de la mochila y/o los pokemons, que se
     * traeran UNICA Y ESPECIFICAMENTE los datos de la partida que se eligiera
     * en el metodo anterior.
     *
     * Mediante el resultado de devolver la "especie" lo comparará para crear un
     * pokemon de esa especie determinada, y luego se introduciran las variables
     * del pokemon.
     *
     * @param con La conexion con la base de datos
     * @param codigo El numero que hace referencia a la partida seleccionada
     * @return La mochila del personaje que se convertira en una algo del
     * personaje mediante el metodo "setMochila".
     * @throws FileNotFoundException Necesario, ya que se manejan variables
     * "File" por la importación de stats desde los archivos ".json".
     */
    public static Mochila cargarMochila(Connection con, int codigo) throws FileNotFoundException {
        Mochila mochila = new Mochila();
        ResultSet result;
        Statement query;
        Pokemon base;
        File file;
        FileReader fr;
        Gson gson = new Gson();
        JsonReader jsonR;
        try {
            query = con.createStatement();
            result = query.executeQuery("SELECT cantidadPokeball FROM Mochilas WHERE id=" + codigo + ";");
            while (result.next()) {
                mochila.cargarPokeball(result.getInt("cantidadPokeball"));
            }
            result = query.executeQuery("SELECT cantidadPocion FROM Mochilas WHERE id=" + codigo + ";");
            while (result.next()) {
                mochila.cargarPociones(result.getInt("cantidadPocion"));
            }

            //Pokemons
            int contador = 0;

            result = query.executeQuery("SELECT nombre,nivel,especie,naturaleza FROM Pokemons WHERE mochila=" + codigo + ";");
            String especie;
            String nombre;
            int nivel;
            Pokemon aux;
            while (result.next()) {
                especie = result.getString("especie");
                nombre = result.getString("nombre");
                nivel = result.getInt("nivel");
                if ("Pikachu".equals(especie)) {
                    file = new File("Pikachu.json");
                    fr = new FileReader(file);
                    jsonR = new JsonReader(fr);
                    base = gson.fromJson(jsonR, Pikachu.class);
                    int salud = base.getSalud();
                    int ataque = base.getAtaque();
                    int defensa = base.getDefensa();
                    int atEspecial = base.getAtEspecial();
                    int defEspecial = base.getDefEspecial();
                    int velocidad = base.getVelocidad();
                    aux = new Pikachu(nivel, nombre, salud, ataque, defensa, atEspecial, defEspecial, velocidad);
                    aux.setNaturaleza(result.getInt("naturaleza"));
                    mochila.cargarPokemon(contador, aux);
                }
                if ("Pidgey".equals(especie)) {
                    file = new File("Pidgey.json");
                    fr = new FileReader(file);
                    jsonR = new JsonReader(fr);
                    base = gson.fromJson(jsonR, Pidgey.class);
                    int salud = base.getSalud();
                    int ataque = base.getAtaque();
                    int defensa = base.getDefensa();
                    int atEspecial = base.getAtEspecial();
                    int defEspecial = base.getDefEspecial();
                    int velocidad = base.getVelocidad();
                    aux = new Pidgey(nivel, nombre, salud, ataque, defensa, atEspecial, defEspecial, velocidad);
                    aux.setNaturaleza(result.getInt("naturaleza"));
                    mochila.cargarPokemon(contador, aux);
                }
                if ("Charmander".equals(especie)) {
                    file = new File("Charmander.json");
                    fr = new FileReader(file);
                    jsonR = new JsonReader(fr);
                    base = gson.fromJson(jsonR, Charmander.class);
                    int salud = base.getSalud();
                    int ataque = base.getAtaque();
                    int defensa = base.getDefensa();
                    int atEspecial = base.getAtEspecial();
                    int defEspecial = base.getDefEspecial();
                    int velocidad = base.getVelocidad();
                    aux = new Charmander(nivel, nombre, salud, ataque, defensa, atEspecial, defEspecial, velocidad);
                    aux.setNaturaleza(result.getInt("naturaleza"));
                    mochila.cargarPokemon(contador, aux);
                }
                contador++;
            }

            result.close();
            query.close();
            con.close();

        } catch (SQLException ex) {
            System.out.println("Error en la conexion con pokemon " + ex.toString());
        }
        if (mochila == null) {
            System.out.println("NO HAY MOCHILA");
        }
        return mochila;
    }

    /**
     * Este metodo servirá para guardar la partida, borrando los datos de la
     * partida y guardandolos despues. Se usara el "START TRANSICTION" para
     * evitar que si una de las querys no se realiza correctamente, no se borre
     * o se introduzcan datos a medias
     *
     * @param partida El personaje de la partida
     * @return un atriburo booleano que mostrará el exito del guardado.
     */
    public static boolean guardarPartida(Personaje partida) {
        boolean exito = true;
        int resultado;
        try {
            Connection con = establecerConexion();
            Statement query = con.createStatement();
            ResultSet result;
            result = query.executeQuery("START TRANSACTION;");
            //Borrado + creacion (con las mismas identificaciones o espacio ocupado) = Sobreescribir.
            //Borrado Pokemons
            resultado = query.executeUpdate("DELETE FROM Pokemons WHERE mochila=" + entrenador.getCodigo());

            //Borrado Partida
            resultado = query.executeUpdate("DELETE FROM Mochilas WHERE id=" + entrenador.getCodigo());

            //Creacion de Partida
            resultado = query.executeUpdate("INSERT INTO Mochilas VALUES('" + entrenador.getGenero() + "'," + mochila.getPociones() + ",'"
                    + entrenador.getNombre() + "'," + entrenador.getPokeCuartos() + ","
                    + mochila.getPokeball() + ",'" + entrenador.getFecha() + "',"
                    + entrenador.getCodigo() + ");");

            //Creacion de Pokemons
            for (int i = 0; i < mochila.getContador(); i++) {
                resultado = query.executeUpdate("INSERT INTO Pokemons VALUES(" + i + ",'" + mochila.getEquipo()[i].getMote() + "','"
                        + mochila.getEquipo()[i].getNombre() + "'," + mochila.getEquipo()[i].getNivel() + ","
                        + entrenador.getCodigo() + "," + mochila.getEquipo()[i].getNaturaleza() + ");");
            }
            result = query.executeQuery("COMMIT;");
        } catch (SQLException ex) {
            System.out.println("No se ha podido establecer la conexion a la base de datos: " + ex.toString());
            exito = false;
        }
        return exito;
    }
    /**
     * Este metodo es un print del menú principal, para no alargar mucho el metodo main
     */
    public static void menu() {
        System.out.println("MENU");
        System.out.println("=====");
        System.out.println("0) - Salir");
        System.out.println("1) - Capturar pokemon");
        System.out.println("2) - Listar pokemon");
        System.out.println("3) - Mostrar Perfil");
        System.out.println("4) - Tienda");
        System.out.print("Opcion: ");
    }
    /**
     * Este metodo es un print del menú de la tienda, para no alargar mucho el metodo main
     */
    public static void menuTienda() {
        System.out.println("Bienvenido a la tienda");
        System.out.println("----------------------------");
        System.out.println("Que deseas comprar?");
        System.out.println("");
        System.out.println("1) - Pokeballs. (100$)");
    }
    /**
     * Este metodo sirve para crear los pokemons que has conseguido capturar. Una vez la captura ha sido exitosa
     * , el pokemon se creará, mediante un numero aleatorio, se determinará la especie del Pokemon.
     * 
     * Dependiendo de la especie, se accedera a un archivo json, que tendrá los atributos basicos del pokemon,
     * y dependiendo de su naturaleza y nivel, se moverá en rangos minimos(naturaleza pobre, nivel 1) y máximos
     * (naturaleza Fuerte, nivel 100) para dar las stats finales.
     * @return 
     */
    public static Pokemon capturarPkm() {
        Pokemon temporal = null;
        try {
            Pokemon base;
            File file;
            FileReader fr;
            Gson gson = new Gson();
            JsonReader jsonR;
            String mote = "null";
            int nivel = (int) (Math.random() * 101 + 1);

            int aleatorio = (int) (Math.random() * 3 + 1);
            switch (aleatorio) {
                case 1:
                    file = new File("Pikachu.json");
                    fr = new FileReader(file);
                    jsonR = new JsonReader(fr);
                    base = gson.fromJson(jsonR, Pikachu.class);
                    int salud = base.getSalud();
                    int ataque = base.getAtaque();
                    int defensa = base.getDefensa();
                    int atEspecial = base.getAtEspecial();
                    int defEspecial = base.getDefEspecial();
                    int velocidad = base.getVelocidad();

                    temporal = new Pikachu(nivel, salud, ataque, defensa, atEspecial, defEspecial, velocidad);

                    break;
                case 2:
                    file = new File("Charmander.json");
                    fr = new FileReader(file);
                    jsonR = new JsonReader(fr);
                    base = gson.fromJson(jsonR, Charmander.class);
                    salud = base.getSalud();
                    ataque = base.getAtaque();
                    defensa = base.getDefensa();
                    atEspecial = base.getAtEspecial();
                    defEspecial = base.getDefEspecial();
                    velocidad = base.getVelocidad();

                    temporal = new Charmander(nivel, salud, ataque, defensa, atEspecial, defEspecial, velocidad);
                    break;
                default:
                    file = new File("Pidgey.json");
                    fr = new FileReader(file);
                    jsonR = new JsonReader(fr);
                    base = gson.fromJson(jsonR, Pidgey.class);
                    salud = base.getSalud();
                    ataque = base.getAtaque();
                    defensa = base.getDefensa();
                    atEspecial = base.getAtEspecial();
                    defEspecial = base.getDefEspecial();
                    velocidad = base.getVelocidad();
                    temporal = new Pidgey(nivel, salud, ataque, defensa, atEspecial, defEspecial, velocidad);
                    break;
            }
            temporal.setNaturaleza((int) (Math.random() * 3 + 1));
            return temporal;
        } catch (FileNotFoundException ex) {
            System.out.println("error al cargar las estadisticas desde " + file.getName() + ": " + ex.toString());
        }
        return temporal;
    }
    /**
     * Este metodo se usará para ordenar los pokemons en función de su nivel (de mayor a menor).
     * @param equipo Los pokemons de la partida
     * @param contador El numero de pokemons del jugador.
     */
    public static void ordenarNivel(Pokemon[] equipo, int contador) {
        for (int i = 0; i <= contador - 1; i++) {
            for (int j = contador - 1; j > i; j--) {
                if (equipo[j].getNivel() > equipo[j - 1].getNivel()) {
                    Pokemon aux = equipo[j];
                    equipo[j] = equipo[j - 1];
                    equipo[j - 1] = aux;
                }
            }
        }
    }
    /**
     * Este metodo profundizará en las stats del pokemon, mostrando mas stats que en la lista de los pokemons
     * listados.
     * @param pkmMostrar La mochila del jugador, donde estan los pokemons.
     */
    public static void mostrarPkm(Mochila pkmMostrar) {
        System.out.println("Escriba la posicion del pokemon para mostrar: ");
        int posicion = input.nextInt();
        Pokemon temporal = pkmMostrar.getPokemon(posicion - 1);

        System.out.println(temporal.getMote());
        System.out.println("===============");
        System.out.println("Especie: " + temporal.getEspecie());
        System.out.println("Nivel: " + temporal.getNivel());
        System.out.println("Tipo: " + temporal.getTipo());
        System.out.println("PS: " + temporal.getSalud());
        System.out.println("Ataque: " + temporal.getAtaque());
        System.out.println("Defensa: " + temporal.getDefensa());
        System.out.println("At. Especial: " + temporal.getAtEspecial());
        System.out.println("Def. Especial: " + temporal.getDefEspecial());
        System.out.println("Velocidad: " + temporal.getVelocidad());

    }

}
