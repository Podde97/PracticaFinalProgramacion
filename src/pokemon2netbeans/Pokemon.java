package pokemon2netbeans;

import java.util.Arrays;

/**
 * La clase abstracta pokemon, es la clase de la que cada uno de los pokemons hereda sus variables y metodos.
 * @author Miguel Dominguez
 */
public abstract class Pokemon {

    protected int naturaleza;
    protected int salud;
    protected int ataque;
    protected int defensa;
    protected int atEspecial;
    protected int defEspecial;
    protected int velocidad;
    protected int nivel;
    protected String nombre;
    protected String especie;
    protected String[] tipo;
    protected String mote;
     /**
      * retorna la salud del pokemon.
      * @return 
      */
    public int getSalud() {
        return salud;
    }
     /**
      * retorna el ataque especial del pokemon.
      * @return 
      */
    public int getAtEspecial() {
        return atEspecial;
    }
     /**
      * retorna la defensa especial del pokemon.
      * @return 
      */
    public int getDefEspecial() {
        return defEspecial;
    }
     /**
      * retorna la velocidad del pokemon.

      * @return 
      */
    public int getVelocidad() {
        return velocidad;
    }
     /**
      * retorna el ataque del pokemon.
      * @return 
      */
    public int getAtaque() {
        return ataque;
    }
     /**
      * retorna la defensa del pokemon.

      * @return 
      */
    public int getDefensa() {
        return defensa;
    }
     /**
      * retorna el nivel del pokemon.
      * @return 
      */
    public int getNivel() {
        return nivel;
    }
     /**
      * retorna el nombre
      * @return 
      */
    public String getNombre() {
        return nombre;
    }
     /**
      * retorna la especie  del pokemon.
      * @return 
      */
    public String getEspecie() {
        return especie;
    }
     /**
      * retorna el/los tipo/s del pokemon.
      * @return 
      */
    public String getTipo() {
        return Arrays.toString(this.tipo);
    }
     /**
      * returna el mote del pokemon.
      * @return 
      */
    public String getMote() {
        return mote;
    }
     /**
      * metodo que determina el mote del pokemon.
      * @param mote 
      */
    public void setMote(String mote) {
        this.mote = mote;
    }
     /**
      * retorna el nombre de la clase (especie) del pokemon.
      * @return 
      */
    public String nombreClase() {
        return this.getClass().getSimpleName();
    }
    /**
     * retorna la naturaleza del pokemon.
     * @return 
     */
    public int getNaturaleza() {
        return naturaleza;
    }
    /**
     * metodo que carga la naturaleza (desde la base de datos).
     * @param naturaleza 
     */
    public void setNaturaleza(int naturaleza) {
        this.naturaleza = naturaleza;
    }
    
}
